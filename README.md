# Examples of running REST commands on Elasticsearch

Full documentation @ [official documentation](https://www.elastic.co/guide/en/kibana/7.2/index.html)

## Running with curl
```shell
curl -XGET -s http://10.10.46.129:9200/_cat/nodes
```

## Running within Kibana

Just paste the command (REST verb + url) to run.
![](images/2020-03-31_10-35.png)

## Some useful command ;]

### System info
```json
GET _all/_settings
GET _cluster/settings
```

### Overall HEALTH
```json
GET _cat/health?pretty
GET _cluster/health?pretty
GET _cat/nodes
GET _cat/shards?v
GET _nodes/stats/fs?human
```

### DEBUG
```json
GET _cluster/health?pretty
GET _cluster/allocation/explain?pretty
GET _cat/allocation?v
```

### Reasign Replicas
```json
POST _cluster/reroute?retry_failed=true

GET _cat/shards?h=index,shard,prirep,node,state,unassigned.reason
GET _cat/shards?h=index,shard,prirep,state,unassigned.reason
GET _cat/shards?h=index,store

GET _cat/indices?h=index

POST _cluster/reroute?retry_failed=true
{"commands":[
    {"allocate_replica":
      {
        "index":"varnish_proxyapp01-20190730",
        "shard":0,
        "node":"9a6c30c1d8ba"
      }
    }
  ]
}
```

### Allocation
```json
GET _cluster/allocation/explain
GET _cat/recovery?v&h=i,s,t,ty,st,shost,thost,f,fp,b,bp
```

### shards stuff
```json
GET _cat/templates/
GET _template
GET /varnish_proxy02-20190721
```

### Remove index
```json
DELETE varnish_proxy01-20190728
DELETE varnish_proxy01-20190729
DELETE varnish_proxy02-20190729
DELETE varnish_proxyapp02-20190729
```

### Rollups
```json
GET varnish-15d-rolled/
GET _rollup/job/_all
GET _rollup/job/varnish_15d_rollup

GET _rollup/job/rollup-varnish_proxy01-20190719
POST _rollup/job/rollup-varnish_proxy01-20190719/_start

DELETE rollup-varnish_proxy01-20190724
DELETE rollup-varnish_proxy02-20190724
DELETE rollup-varnish_proxy01-20190719

DELETE varnish-15d-rolled

GET /varnish_proxy*-20190710
```

### Snapshots Info
```json
 GET _snapshot
 GET _snapshot/_all
 GET /_snapshot/my_backup/snapshot_1/_current
 GET _snapshot/varnish-snap/snapshot-20190823/_status
```

#### Create snapshot
```json
PUT _snapshot/varnish-snap/snapshot-20191007
{
  "indices": "varnish_proxy01-20191007,varnish_proxy02-20191007,varnish_proxyapp01-20191007,varnish_proxyapp02-20191007",
  "ignore_unavailable": true,
  "include_global_state": true
}
```

### Delete snapshot
```json
DELETE _snapshot/varnish-snap/snapshot-20191001
```

### Delete Index
```json
DELETE /varnish_proxy01-20190911
```

### Disabling node from cluster
```json
PUT _cluster/settings
{"transient":{"cluster.routing.allocation.exclude._name":"els-master-d"}}
```

### Enabling node from cluster
```json
PUT _cluster/settings
{"transient":{"cluster.routing.allocation.include._name":""}}
```

### Circuit Breakers - field data
```json
GET /_cluster/settings?include_defaults=true
```

### Restore shards and snapshots

#### delete index
```json
DELETE /varnish_proxy01-20200316
```

#### restore index from snap
```json
POST /_snapshot/varnish-snap/snapshot-20200316/_restore
{
  "indices": "varnish_proxy01-20200316",
  "ignore_unavailable": true,
  "include_global_state": true
}
```

#### and then reroute
```json
POST _cluster/reroute?retry_failed=true
```

### Troubleshooting

#### Problem 001

Symptom: Indices em read only, não podendo ser incrementados.

Error message:
```shell
[cluster_block_exception] index [varnish-20200819] blocked by: [FORBIDDEN/12/index read-only / allow delete (api)];
```

Fix: 
```json
PUT your_index_name/_settings
{
  "index": {
    "blocks": {
      "read_only_allow_delete": "false"
    }
  }
}
```

Validate:
```json
GET your_index_name/_settings
```

Article: [link](https://discuss.elastic.co/t/forbidden-12-index-read-only-allow-delete-api/110282/3)

#### Problem XXX